# Xente Ebook Seller Application

**_This is application is being worked on by the Xente team for the bootcamp_**

> It is intended to allow a buyer to pay for the ebook using mobile money
> The task is to develop an SDK to consume the Xente API to allow easy and effortless integration of the the API.
> The following are the team members who are developing the SDK
>
> 1. Oketa Fred
> 2. Olive Nakiyemba
> 3. Oketayot Julius Peter
> 4. Kintu Declan Trevor

The application can be accessed live here [Xente Book Seller](https://xentebookseller.herokuapp.com/)
